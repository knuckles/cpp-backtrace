#include <libunwind.h>
#include <dlfcn.h>
#include <cxxabi.h>

#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>

#include <fmt/format.h>

namespace {

std::vector<std::string> backtrace() {
  unw_cursor_t cursor;
  unw_context_t context;

  // Initialize cursor to current frame for local unwinding.
  unw_getcontext(&context);
  unw_init_local(&cursor, &context);

  std::vector<std::string> result;
  // Unwind frames one by one, going up the frame stack.
  while (unw_step(&cursor) > 0) {
    unw_word_t offset, pc;
    unw_get_reg(&cursor, UNW_REG_IP, &pc);
    if (pc == 0) {
      break;
    }

    result.push_back(fmt::format("0x%lx:", pc));

    char sym[256];
    if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) {
      char* nameptr = sym;
      int status;
      char* demangled = abi::__cxa_demangle(sym, nullptr, nullptr, &status);
      if (status == 0) {
        nameptr = demangled;
      }
      result.push_back(fmt::format(" (%s+0x%lx)", nameptr, offset));
      std::free(demangled);
    } else {
      result.push_back(" -- error: unable to obtain symbol name for this frame");
    }
  }

  return result;
}

std::vector<std::string> last_trace;

extern "C" {

void __cxa_throw(void* ex, std::type_info* info, void (* dest)(void *)) {
  last_trace = backtrace();

  static void (*const rethrow)(void*,void*,void(*)(void*)) /*__attribute__ ((noreturn))*/ =
    (void(*)(void*,void*,void(*)(void*))) dlsym(RTLD_NEXT, "__cxa_throw");
  rethrow(ex, info, dest);
}

}  // extern "C"
}  // namespace


class MyError : public std::runtime_error {
public:
  MyError(const char* what)
    : std::runtime_error(what) {}
};

void foo(int x) {
  if (x < 10)
    throw MyError("rt err0");
}

int main(int argc, char** argv) {
  try {
    foo(5);
  }
  catch(const std::exception& e) {
    std::cerr << e.what() << '\n';
    for (const auto& line : last_trace) {
      std::cerr << line << '\n';
    }
  }
  return 0;
}
